%global snapdate @SNAPSHOTDATE@
%global commit0 @COMMITHASH0@
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

Name:           kicad-nightly-doc
Version:        @VERSION@
Release:        1.%{snapdate}git%{shortcommit0}%{?dist}
Summary:        Documentation for KiCad
License:        GPLv3+ or CC-BY
URL:            https://docs.kicad.org/

Source0:        https://gitlab.com/kicad/services/kicad-doc/-/archive/%{commit0}/kicad-doc-%{commit0}.tar.gz

BuildArch:      noarch

BuildRequires:  cmake
BuildRequires:  make
BuildRequires:  po4a
BuildRequires:  rubygem-asciidoctor

Recommends:     kicad-nightly

%description
KiCad is an open-source electronic design automation software suite for the
creation of electronic schematic diagrams and printed circuit board artwork.
This package provides the documentation for the nightly development build of
KiCad in multiple languages.


%prep

%autosetup -n kicad-doc-%{commit0}


%build

# HTML only
%cmake \
    -DKICAD_DOC_PATH=%{_docdir}/kicad-nightly/help \
    -DBUILD_FORMATS=html
%cmake_build


%install

%cmake_install


%files
%{_docdir}/kicad-nightly/help/
%license LICENSE.adoc LICENSE.CC-BY LICENSE.GPLv3


%changelog
