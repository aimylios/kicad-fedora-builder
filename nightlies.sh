#!/bin/sh

set -e
set -x

dnf install copr-cli curl jq -y
dnf remove python3-progress --noautoremove -y
./builder.sh -c "@kicad/kicad"

exit 0
