#!/bin/sh

set -e
set -x

# GitLab Project IDs.
GITLAB_ID_KICAD=15502567	# https://gitlab.com/kicad/code/kicad
GITLAB_ID_TEMPLATES=21506275	# https://gitlab.com/kicad/libraries/kicad-templates
GITLAB_ID_SYMBOLS=21545491	# https://gitlab.com/kicad/libraries/kicad-symbols
GITLAB_ID_FOOTPRINTS=21601606	# https://gitlab.com/kicad/libraries/kicad-footprints
GITLAB_ID_PACKAGES3D=21604637	# https://gitlab.com/kicad/libraries/kicad-packages3D
GITLAB_ID_DOC=15621628		# https://gitlab.com/kicad/services/kicad-doc

# GitLab API URL prefix and suffix (to be combined with Project ID).
GITLAB_API_PREFIX="https://gitlab.com/api/v4/projects/"
GITLAB_API_SUFFIX="/repository/commits/master"

# The version of the packages will be set to the current KICAD_SEMANTIC_VERSION
# defined in the master branch of the KiCad source code.  This is the URL to the
# file where the corresponding CMake variable is defined.
KICAD_VERSION_URL="https://gitlab.com/kicad/code/kicad/-/raw/master/cmake/KiCadVersion.cmake"

# Get the date of the HEAD commit in a repo.
get_last_date () {
	local PROJECT_ID="$1"
	local API_URL="${GITLAB_API_PREFIX}${PROJECT_ID}${GITLAB_API_SUFFIX}"
	sleep 1
	local LAST_DATE="$(curl -s ${API_URL} | jq -r '.created_at')"
	echo "$(date --utc --date=${LAST_DATE} +%Y%m%d)"
}

# Get the SHA of the HEAD commit in a repo.
get_last_rev () {
	local PROJECT_ID="$1"
	local API_URL="${GITLAB_API_PREFIX}${PROJECT_ID}${GITLAB_API_SUFFIX}"
	sleep 1
	echo "$(curl -s ${API_URL} | jq -r '.id')"
}

show_help () {
	set +x
	echo "$0 [-c COPR_ID] [-m MOCK_TARGET] [-f] [-h]" >&2
	echo "" >&2
	echo "  -h shows this help message." >&2
	echo "  -c COPR_ID performs a remote COPR build using the specified ID." >&2
	echo "  -m MOCK_TARGET performs a local MOCK build for the specified target." >&2
	echo "  -f forces all packages to be (re-)built." >&2
	echo "" >&2
	echo "If neither -c nor -m is specified, then no build will be done, but" >&2
	echo "the SPECs will still be prepared, and can be used to manually kick off" >&2
	echo "a build at a later time." >&2
	echo "" >&2
	echo "example: $0 -c your_copr_id/kicad" >&2
	echo "example: $0 -m fedora-rawhide-x86_64" >&2
	set -x
}

# Start of main shell script.
COPR_ID=
MOCK_TARGET=
BUILD_KICAD=
BUILD_3DMODELS=
BUILD_DOC=
while getopts ":hc:m:f" opt; do
	case "$opt" in
		h)
			show_help
			exit 1
			;;
		c)
			COPR_ID="${OPTARG}"
			;;
		m)
			MOCK_TARGET="${OPTARG}"
			;;
		f)
			BUILD_KICAD="TRUE"
			BUILD_3DMODELS="TRUE"
			BUILD_DOC="TRUE"
			;;
		\?)
			set +x
			echo "Invalid option: -${OPTARG}" >&2
			set -x
			show_help
			exit 1
			;;
		:)
			set +x
			echo "Option -${OPTARG} requires an argument." >&2
			set -x
			show_help
			exit 1
			;;
	esac
done

# Prepare directories.
ARTIFACTS=build/artifacts
RPMBUILD=build/rpmbuild
mkdir -p ${ARTIFACTS}
mkdir -p ${RPMBUILD}/{SOURCES,SPECS,SRPMS}

# Get current version of KiCad master branch.
KICAD_VERSION_FILE="$(curl -s ${KICAD_VERSION_URL})"
KICAD_VERSION="$(grep -m 1 "KICAD_SEMANTIC_VERSION" <<< ${KICAD_VERSION_FILE} | grep -o -E "[[:digit:]\.]+" | head -n 1)"

# Query information about latest commits.
SNAPDATE_KICAD=$(get_last_date ${GITLAB_ID_KICAD})
COMMIT_KICAD=$(get_last_rev ${GITLAB_ID_KICAD})
COMMIT_TEMPLATES=$(get_last_rev ${GITLAB_ID_TEMPLATES})
COMMIT_SYMBOLS=$(get_last_rev ${GITLAB_ID_SYMBOLS})
COMMIT_FOOTPRINTS=$(get_last_rev ${GITLAB_ID_FOOTPRINTS})
SNAPDATE_3DMODELS=$(get_last_date ${GITLAB_ID_PACKAGES3D})
COMMIT_3DMODELS=$(get_last_rev ${GITLAB_ID_PACKAGES3D})
SNAPDATE_DOC=$(get_last_date ${GITLAB_ID_DOC})
COMMIT_DOC=$(get_last_rev ${GITLAB_ID_DOC})

# Check if current revisions were already built before.
grep -q "${COMMIT_KICAD}" ${RPMBUILD}/SPECS/* || BUILD_KICAD="TRUE"
grep -q "${COMMIT_3DMODELS}" ${RPMBUILD}/SPECS/* || BUILD_3DMODELS="TRUE"
grep -q "${COMMIT_DOC}" ${RPMBUILD}/SPECS/* || BUILD_DOC="TRUE"

# (Re-)Create the SPEC files.
set +x
echo "Generating SPECs." >&2
set -x

# The version of the packages follows the Fedora Packaging Guidelines and will
# be set to <apprel>-<pkgrel>.<snapinfo> (e.g., 5.99.0-1.20210227git061218e).
# When multiple different snapshots from the same date are built, the order of
# versions is determined by the commit hash. This can lead to a situation where
# a newer build is not detected as an update for an older build. More complex
# logic would have to be implemented to detect such a case and bump <pkgrel> if
# required and automatically reset it back to "1" when <apprel> is increased.

SPEC_KICAD="${RPMBUILD}/SPECS/kicad-nightly.spec"
sed \
	-e "s/@SNAPSHOTDATE@/${SNAPDATE_KICAD}/" \
	-e "s/@COMMITHASH0@/${COMMIT_KICAD}/" \
	-e "s/@COMMITHASH1@/${COMMIT_TEMPLATES}/" \
	-e "s/@COMMITHASH2@/${COMMIT_SYMBOLS}/" \
	-e "s/@COMMITHASH3@/${COMMIT_FOOTPRINTS}/" \
	-e "s/@VERSION@/${KICAD_VERSION}/" \
	templates/kicad-nightly.spec > ${SPEC_KICAD}
set +x
echo "Prepared ${SPEC_KICAD}." >&2
set -x

SPEC_3DMODELS="${RPMBUILD}/SPECS/kicad-nightly-packages3d.spec"
sed \
	-e "s/@SNAPSHOTDATE@/${SNAPDATE_3DMODELS}/" \
	-e "s/@COMMITHASH0@/${COMMIT_3DMODELS}/" \
	-e "s/@VERSION@/${KICAD_VERSION}/" \
	templates/kicad-nightly-packages3d.spec > ${SPEC_3DMODELS}
set +x
echo "Prepared ${SPEC_3DMODELS}." >&2
set -x

SPEC_DOC="${RPMBUILD}/SPECS/kicad-nightly-doc.spec"
sed \
	-e "s/@SNAPSHOTDATE@/${SNAPDATE_DOC}/" \
	-e "s/@COMMITHASH0@/${COMMIT_DOC}/" \
	-e "s/@VERSION@/${KICAD_VERSION}/" \
	templates/kicad-nightly-doc.spec > ${SPEC_DOC}
set +x
echo "Prepared ${SPEC_DOC}." >&2
set -x

# Do a local mock build.
if [ -n "${MOCK_TARGET}" ]; then
	set +x
	echo "Generating SRPMs."
	set -x

	# Remove old source archives and SRPMs.
	ls -1 ${RPMBUILD}/SOURCES/ | \
	while read SOURCE; do
		SRCNAME=${SOURCE%%.*}
		HASH=${SRCNAME##*-}
		if ! grep -q "${HASH}" ${RPMBUILD}/SPECS/*; then
			rm -f ${RPMBUILD}/SOURCES/${SOURCE}
		fi
	done
	rm -f ${RPMBUILD}/SRPMS/*

	rpmbuild --define="_topdir ${RPMBUILD}" --undefine=_disable_source_fetch -bs "${SPEC_KICAD}"
	SRPM_KICAD=$(find ${RPMBUILD}/SRPMS/ -name "kicad-nightly-${KICAD_VERSION}*.src.rpm")
	set +x
	echo "Prepared ${SRPM_KICAD}."
	set -x

	rpmbuild --define="_topdir ${RPMBUILD}" --undefine=_disable_source_fetch -bs "${SPEC_3DMODELS}"
	SRPM_3DMODELS=$(find ${RPMBUILD}/SRPMS/ -name "kicad-nightly-packages3d-${KICAD_VERSION}*.src.rpm")
	set +x
	echo "Prepared ${SRPM_3DMODELS}."
	set -x

	rpmbuild --define="_topdir ${RPMBUILD}" --undefine=_disable_source_fetch -bs "${SPEC_DOC}"
	SRPM_DOC=$(find ${RPMBUILD}/SRPMS/ -name "kicad-nightly-doc-${KICAD_VERSION}*.src.rpm")
	set +x
	echo "Prepared ${SRPM_DOC}."
	set -x

	set +x
	echo "Starting the local mock build."
	set -x

	MOCK_SRPMS=

	if [ -n "${BUILD_KICAD}" ]; then
		MOCK_SRPMS="${MOCK_SRPMS} ${SRPM_KICAD}"
	else
		set +x
		echo "Package kicad-nightly-${KICAD_VERSION}-1.${SNAPDATE_KICAD}git${COMMIT_KICAD:0:7} was already built."
		set -x
	fi

	if [ -n "${BUILD_3DMODELS}" ]; then
		MOCK_SRPMS="${MOCK_SRPMS} ${SRPM_3DMODELS}"
	else
		set +x
		echo "Package kicad-nightly-packages3d-${KICAD_VERSION}-1.${SNAPDATE_3DMODELS}git${COMMIT_3DMODELS:0:7} was already built."
		set -x
	fi

	if [ -n "${BUILD_DOC}" ]; then
		MOCK_SRPMS="${MOCK_SRPMS} ${SRPM_DOC}"
	else
		set +x
		echo "Package kicad-nightly-doc-${KICAD_VERSION}-1.${SNAPDATE_DOC}git${COMMIT_DOC:0:7} was already built."
		set -x
	fi

	if [ -n "${MOCK_SRPMS}" ]; then
		mock -r "${MOCK_TARGET}" --resultdir="${ARTIFACTS}" --rebuild ${MOCK_SRPMS}
	fi
fi

# Do a remote copr build.  We allow 20 hours.  We shouldn't need that much,
# but the default of 5 hours is too short for the new zstd compression.
if [ -n "${COPR_ID}" ]; then
	set +x
	STATUS_LOCATION=$(echo ${COPR_ID} | sed -e 's/@/g\//')
	echo "Starting the remote copr build.  Check the status of the build here:"
	echo "https://copr.fedoraproject.org/coprs/${STATUS_LOCATION}/builds/"
	set -x

	if [ -n "${BUILD_KICAD}" ]; then
		copr-cli build --timeout 72000 --nowait "${COPR_ID}" "${SPEC_KICAD}"
	else
		set +x
		echo "Package kicad-nightly-${KICAD_VERSION}-1.${SNAPDATE_KICAD}git${COMMIT_KICAD:0:7} was already built."
		set -x
	fi

	if [ -n "${BUILD_3DMODELS}" ]; then
		copr-cli build --timeout 72000 --nowait "${COPR_ID}" "${SPEC_3DMODELS}"
	else
		set +x
		echo "Package kicad-nightly-packages3d-${KICAD_VERSION}-1.${SNAPDATE_3DMODELS}git${COMMIT_3DMODELS:0:7} was already built."
		set -x
	fi

	if [ -n "${BUILD_DOC}" ]; then
		copr-cli build --timeout 72000 --nowait "${COPR_ID}" "${SPEC_DOC}"
	else
		set +x
		echo "Package kicad-nightly-doc-${KICAD_VERSION}-1.${SNAPDATE_DOC}git${COMMIT_DOC:0:7} was already built."
		set -x
	fi
fi

exit 0
