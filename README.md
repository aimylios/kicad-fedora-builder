# Nightly builds of KiCad for Fedora

KiCad is a cross platform and open source Electronic Design Automation suite. Please visit the [official KiCad website](https://kicad.org/) for details.

This repository contains the SPEC description files and packaging scripts to generate nightly build RPMs of KiCad for Fedora. The ready-made packages are available via the [@kicad/kicad](https://copr.fedorainfracloud.org/coprs/g/kicad/kicad/) Copr repository. They are rebuilt automatically on a daily basis in case new commits have been added since the last build.

These packages can be installed in parallel to the stable release from the official repository. This makes it possible for Fedora users to try out the new features of the development snapshot and at the same time continue to work on their projects with the stable version.

## Installation

If you don't have Copr yet, install it with:

```shell
$ sudo dnf install dnf-plugins-core
```

Enable the Copr repository:

```shell
$ sudo dnf copr enable @kicad/kicad
```

Install KiCad:

```shell
$ sudo dnf install kicad-nightly
```

The kicad-nightly package includes the latest revision of all libraries except for the 3D models from the [kicad-packages3D repository](https://gitlab.com/kicad/libraries/kicad-packages3D/). Because of their size of more than 5 GB on disk they have been moved to a separate package and can be installed with:

```shell
$ sudo dnf install kicad-nightly-packages3d
```

Install the documentation:

```shell
$ sudo dnf install kicad-nightly-doc
```

## Known issues and limitations

### File type associations

The nightly and the release package register the same file extensions. If you have both of them installed in parallel, your file manager will therefore not know which application to use to open KiCad files (\*.kicad_pro, \*.kicad_sch, \*.kicad_pcb, etc.), and it may arbitrarily choose the one or the other. To be sure to always open a project with the desired version, we recommend to first launch the correct application and then use the *File -> Open...* menu entry.

### Python scripting

The Pcbnew Python scripting functionality works from within the KiCad PCB Editor (i.e., Scripting Console and Action Plugins). But it does not work out-of-the-box when launching a standalone Python interpreter. Calling `import pcbnew` will most likely lead to one of the following error messages:

```
ModuleNotFoundError: No module named ‘pcbnew’
```

```
ImportError: libkicad_3dsg.so.2.0.0: cannot open shared object file: No such file or directory
```

This can be worked around by setting the LD_LIBRARY_PATH and PYTHONPATH environment variables before launching the Python interpreter (or by applying some tricks from within your script before importing the pcbnew module). E.g., on Fedora 36 x86_64:

```shell
$ export LD_LIBRARY_PATH=/usr/lib64/kicad-nightly/lib64/
$ export PYTHONPATH=/usr/lib64/kicad-nightly/lib64/python3.10/site-packages/
```

When the stable release is installed in parallel, `import pcbnew` will import its version of the Python binding instead (because they are located in the default path).

### Debugging

The commands to launch the nightly version of the KiCad applications are `kicad-nightly`, `eeschema-nightly`, `pcbnew-nightly`, etc. But the binaries themselves are installed in a non-standard location, which is */usr/lib64/kicad-nightly/bin/*. So if you want to debug the applications or get a backtrace for a bug report, you will have to make sure that the LD_LIBRARY_PATH environment variable is set correctly (see above) and then point the debugger directly to the binary (e.g., `gdb /usr/lib64/kicad-nightly/bin/kicad` instead of `gdb kicad-nightly`).
