%global snapdate @SNAPSHOTDATE@
%global commit0 @COMMITHASH0@
%global commit1 @COMMITHASH1@
%global commit2 @COMMITHASH2@
%global commit3 @COMMITHASH3@
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

%global kicad_prefix %{_libdir}/kicad-nightly

# We need to strip the leading "/usr/" from the python3_sitearch macro,
# and use that as the location of our python files.
%global kicad_python_path %{lua:
    -- Anchor the match to the beginning of the string.
    --
    -- We also want to swallow the path separator so we wind up with
    -- a relative path.
    local prefix = "^" .. rpm.expand("%{_prefix}") .. "/"
    local python3_sitearch = rpm.expand("%{python3_sitearch}")
    local result = string.gsub(python3_sitearch, prefix, "")
    print(result)
}

Name:           kicad-nightly
Version:        @VERSION@
Release:        1.%{snapdate}git%{shortcommit0}%{?dist}
Summary:        EDA software suite for creation of schematic diagrams and PCBs
License:        GPLv3+
URL:            https://kicad.org/

Source0:        https://gitlab.com/kicad/code/kicad/-/archive/%{commit0}/kicad-%{commit0}.tar.gz
Source1:        https://gitlab.com/kicad/libraries/kicad-templates/-/archive/%{commit1}/kicad-templates-%{commit1}.tar.gz
Source2:        https://gitlab.com/kicad/libraries/kicad-symbols/-/archive/%{commit2}/kicad-symbols-%{commit2}.tar.gz
Source3:        https://gitlab.com/kicad/libraries/kicad-footprints/-/archive/%{commit3}/kicad-footprints-%{commit3}.tar.gz

# https://gitlab.com/kicad/code/kicad/-/issues/237
ExclusiveArch:  %{ix86} x86_64 %{arm} aarch64 ppc64le

BuildRequires:  boost-devel
BuildRequires:  cmake
BuildRequires:  desktop-file-utils
BuildRequires:  gcc-c++
BuildRequires:  gettext
BuildRequires:  glew-devel
BuildRequires:  glm-devel
BuildRequires:  gtk3-devel
BuildRequires:  libappstream-glib
BuildRequires:  libcurl-devel
BuildRequires:  libngspice-devel
BuildRequires:  make
BuildRequires:  opencascade-devel
BuildRequires:  python3-devel
BuildRequires:  python3-wxpython4
BuildRequires:  shared-mime-info
BuildRequires:  swig
BuildRequires:  unixODBC-devel
BuildRequires:  wxGTK-devel
BuildRequires:  zlib-devel

Provides:       bundled(fmt) = 9.0.0
Provides:       bundled(libdxflib) = 3.26.4
Provides:       bundled(polyclipping) = 6.4.2
Provides:       bundled(potrace) = 1.15

Requires:       electronics-menu
Requires:       libngspice
Requires:       ngspice-codemodel
Requires:       python3-wxpython4
Requires:       unixODBC

Recommends:     redhat-lsb-core

Suggests:       kicad

%description
KiCad is an open-source electronic design automation software suite for the
creation of electronic schematic diagrams and printed circuit board artwork.
This package provides a nightly development build of KiCad as well as the
drawing sheets, project templates, schematic symbols and PCB footprints which
are part of the official KiCad libraries. It can be installed in parallel to the
stable release package. Nightly builds are untested, might be affected by
serious bugs and/or produce files that are incompatible with the latest stable
release. This can potentially lead to a corruption or even loss of data. Always
take a backup of your files before opening them with the applications from this
package.


%prep

%setup -q -n kicad-%{commit0} -a 1 -a 2 -a 3

# set the version of the application to the version of the package
sed -i 's/-unknown/-%{release}/g' cmake/KiCadVersion.cmake


%build

# KiCad application
%cmake \
    -DKICAD_SCRIPTING_WXPYTHON=ON \
    -DKICAD_USE_OCC=ON \
    -DKICAD_INSTALL_DEMOS=ON \
    -DKICAD_BUILD_QA_TESTS=OFF \
    -DKICAD_BUILD_I18N=ON \
    -DKICAD_I18N_UNIX_STRICT_PATH=ON \
    -DKICAD_USE_EGL=OFF \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
    -DCMAKE_INSTALL_PREFIX=%{kicad_prefix} \
    -DCMAKE_INSTALL_DATADIR=%{_datadir} \
    -DCMAKE_INSTALL_DOCDIR=%{_docdir} \
    -DDEFAULT_INSTALL_PATH=%{kicad_prefix} \
    -DKICAD_DATA=%{_datadir}/%{name} \
    -DKICAD_DOCS=%{_docdir}/%{name} \
    -DPYTHON_SITE_PACKAGE_PATH=%{kicad_python_path}
%cmake_build

# drawing sheets and project templates
pushd kicad-templates-%{commit1}/
%cmake \
    -DKICAD_DATA=%{_datadir}/%{name}
%cmake_build
popd

# schematic symbol libraries
pushd kicad-symbols-%{commit2}/
%cmake \
    -DKICAD_DATA=%{_datadir}/%{name}
%cmake_build
popd

# PCB footprint libraries
pushd kicad-footprints-%{commit3}/
%cmake \
    -DKICAD_DATA=%{_datadir}/%{name}
%cmake_build
popd


%install

# KiCad application
%cmake_install
mkdir -p %{buildroot}%{_docdir}/%{name}/
cp -p AUTHORS.txt %{buildroot}%{_docdir}/%{name}/

# Python scripts in non-standard paths require manual byte compilation
%py_byte_compile %{python3} %{buildroot}%{kicad_prefix}/%{_lib}/python%{python3_version}/site-packages/

# wrapper scripts
mkdir -p %{buildroot}%{_bindir}
ls -1 %{buildroot}%{kicad_prefix}/bin/ | grep -v -F '.kiface' | \
    while read application; do
        (
            echo '#!/usr/bin/sh'
            echo ''
            echo 'export LD_LIBRARY_PATH=%{kicad_prefix}/%{_lib}/'
            echo ''
            echo "%{kicad_prefix}/bin/${application} \"\$@\""
        ) > %{buildroot}%{_bindir}/${application}-nightly
    done

# icons
pushd resources/linux/icons-nightly/hicolor/
ls -1 | \
    while read size; do
        cp -p ${size}/apps/* %{buildroot}%{_datadir}/icons/hicolor/${size}/apps/
    done
popd
pushd %{buildroot}%{_datadir}/icons/hicolor/
ls -1 | \
    while read size; do
        ls -1 ${size}/apps/ | \
            while read icon; do
                mv ${size}/apps/${icon} ${size}/apps/${icon%%.*}-nightly.${icon##*.}
            done
        ls -1 ${size}/mimetypes/ | grep 'kicad' | \
            while read icon; do
                mv ${size}/mimetypes/${icon} ${size}/mimetypes/${icon%%%%kicad*}kicad-nightly${icon#*kicad}
            done
    done
popd

# MIME files
pushd %{buildroot}%{_datadir}/mime/packages/
sed -i \
    -e 's/x-kicad/x-kicad-nightly/g' \
    -e 's/KiCad/KiCad Nightly/g' \
    -e 's/weight="[0-9]*"/weight="0"/g' \
    -e 's/priority="[0-9]*"/priority="0"/g' \
    kicad-kicad.xml
ls -1 | grep -F '.xml' | \
    while read mimefile; do
        mv ${mimefile} ${mimefile%%%%-*}-nightly-${mimefile#*-}
    done
popd

# application launchers
pushd %{buildroot}%{_datadir}/applications/
ls -1 | grep -F '.desktop' | \
    while read launcher; do
        sed -i \
            -e 's/^Name\(.*\)=\([^(]*\)$/Name\1=\2 NIGHTLY/g' \
            -e 's/^Name\(.*\)=\(.*\)\( (.*\)$/Name\1=\2 NIGHTLY\3/g' \
            -e 's/^Icon=\(.*\)$/Icon=\1-nightly/g' \
            -e 's/^Exec=\([^ ]*\)\(.*\)$/Exec=\1-nightly\2/g' \
            -e 's/^StartupWMClass=\([^ ]*\)\(.*\)$/StartupWMClass=\1-nightly\2/g' \
            -e 's/^MimeType=\(.*kicad\)\(.*;\)$/MimeType=\1\2\1-nightly\2/g' \
            ${launcher}
        launcher_new=${launcher%%.*}-nightly.desktop
        launcher_new=zzz.${launcher_new:4}
        mv ${launcher} ${launcher_new}
        desktop-file-install \
            --dir %{buildroot}%{_datadir}/applications/ \
            --remove-category Science \
            --delete-original \
            ${launcher_new}
    done
popd

# AppStream metainfo file
pushd %{buildroot}%{_metainfodir}
sed -i \
    -e 's/\(<id>.\+\)\(<\/id>\)$/\1_nightly\2/g' \
    -e 's/\(<name.*>.\+\)\(<\/name>\)$/\1 Nightly\2/g' \
    -e 's/\(<launchable.*>\).\+\(<\/launchable>\)$/\1zzz.kicad.kicad-nightly.desktop\2/g' \
    -e 's/\(<binary>.\+\)\(<\/binary>\)$/\1-nightly\2/g' \
    -e 's/x-kicad/x-kicad-nightly/g' \
    org.kicad.kicad.metainfo.xml
mv org.kicad.kicad.metainfo.xml org.kicad.kicad_nightly.metainfo.xml
popd

# drawing sheets and project templates
pushd kicad-templates-%{commit1}/
%cmake_install
cp -p LICENSE.md ../LICENSE-templates.md
popd

# schematic symbol libraries
pushd kicad-symbols-%{commit2}/
%cmake_install
cp -p LICENSE.md ../LICENSE-symbols.md
popd

# PCB footprint libraries
pushd kicad-footprints-%{commit3}/
%cmake_install
cp -p LICENSE.md ../LICENSE-footprints.md
popd

# create folder for 3D model libraries
mkdir -p %{buildroot}%{_datadir}/%{name}/3dmodels/


%check

appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/*.metainfo.xml


%files
%attr(0755, root, root) %{_bindir}/*
%{_datadir}/%{name}/
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/apps/*.*
%{_datadir}/icons/hicolor/*/mimetypes/application-x-*.*
%{_datadir}/mime/packages/*.xml
%{_docdir}/%{name}/
%{_metainfodir}/*.metainfo.xml
%{kicad_prefix}/
%license LICENSE*


%changelog
